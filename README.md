# productiveInVR

This projects aims to enable software development and testing inside a VR environment.
We are using Three.js to render webgl and ACE code editor and are currently looking at terminal integration and save/load files.

![preview](https://gitlab.com/maidenOne/productiveinvr/wikis/uploads/aac8ff6cea1d0a7e464c68a2fea48dd5/preview.gif)