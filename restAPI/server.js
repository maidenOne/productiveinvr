var express = require("express");
var glob = require("glob")

// options is optional
glob("userFiles/**/*.*", {}, function (er, files) {
  console.log(files);
})

var app = express();

app.get("/url", (req, res, next) => {
  console.log(req)
 res.json(["Tony","Lisa","Michael","Ginger","Food"]);
});

app.listen(3000, () => {
 console.log("Server running on port 3000");
});
